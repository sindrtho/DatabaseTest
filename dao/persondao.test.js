var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql",
  user: "root",
  password: "secret",
  database: "supertestdb",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);

beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});

test("delete user from sb", done => {
    var number = 0;
    personDao.getAll((status, data) => {
        number = data.length;
        personDao.deleteOne(1, idk => {
            personDao.getAll((status, data)=> {
                expect(data.length).toBe(number - 1);
                done();
            });
        });
    });
});

test("update a user in db", done => {
    var name = "";
    personDao.getOne(3, (status, data) => {
        name = data[0].navn;
        var json = {navn: "Sindre", adresse: "Idk", alder: 22};
        personDao.update(3, json, idk => {
            personDao.getOne(3, (status, data) => {
                expect(data[0].navn).not.toBe(name);
                expect(data[0].navn).toBe("Sindre");
                expect(data[0].adresse).toBe("Idk");
                expect(data[0].alder).toBe(22);
                done();
            });
        });
    });
}); 